<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('matricula');
            $table->string('nome');
            $table->string('cpf');
            $table->string('numeroBeneficio');
            $table->string('nascimento');
            $table->string('valorBeneficio');
            $table->string('percentual');
            $table->string('status');
            $table->string('valorDesconto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socio');
    }
}
