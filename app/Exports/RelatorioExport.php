<?php

namespace App\Exports;

use App\Models\Aluno;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RelatorioExport implements FromCollection,WithHeadings
{
    private $turma;

    public function __construct($turma)
    {
        $this->turma = $turma;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Aluno::listaTurma($this->turma);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'id',
            'Turma ID',
            'Status',
            'Matrícula',
            'Nome',
            'Numero Benefício',
            'Código Status',
            'Faltas',
            'Presenças',
        ];
    }
}
