<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Socio;

class SocioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->socio = new Socio();
    }
    
    public function consultaSocio($id){
        $socio = Socio::get($id);
        $mensalidades = Socio::getMensalidades();
        return view('socio', ['socio'=>$socio[0], 'mensalidades'=>$mensalidades['repasse']]);
    }

    public function listaSocio(){
        $socios = Socio::getAll();
        return view('buscaSocio', ["socios"=>$socios]);
    }
}
