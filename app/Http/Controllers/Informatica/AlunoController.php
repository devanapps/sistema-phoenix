<?php

namespace App\Http\Controllers\Informatica;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Aluno;
use App\Models\Socio;
use App\Models\Turma;
use App\Models\Observacao;
use phpDocumentor\Reflection\Types\This;

class AlunoController extends Controller
{
    private $aluno;

    public function __construct()
    {
        $this->middleware('auth');
        $this->aluno = new Aluno();
        $this->socio = new Socio();
        $this->turma = new Turma();
        $this->observacao = new Observacao();
    }

    public function matricularAluno($id)
    {
        $cod_turma = Turma::getCodTurma($id);
        return view('curso.matricula', ['cod_turma' => $cod_turma[0], 'id' => $id]);
    }

    public function transferirAluno($id, $turma)
    {
        if (Aluno::transferir($id, $turma)) {
            return redirect('curso/turma/' . $turma)->with("success", 'Aluno transferido');
        }
    }

    public function buscarAluno(Request $request)
    {
        // busca as informações na tabela de socio
        $aluno = Socio::getSocio($request->matricula);

        return json_encode($aluno);
    }

    public function salvarAluno(Request $request)
    {
        /* TODO 
            fazer validações
            1 - verificar se está ativo
            2 - verificar se não está em outra turma
        */
        $check = Aluno::verificaAluno($request->matriculaSocio);

        if (sizeof($check) > 0) {
            return redirect('curso/matricularAluno/' . $request->turmaId)->with("error", 'Aluno já matriculado em uma turma');
        } else {
            $aluno = Aluno::create($request->all());
            return redirect('curso/matricularAluno/' . $request->turmaId)->with("success", 'Aluno Matriculado');
        }
    }

    public function detalheAluno($id)
    {
        $aluno = Aluno::buscaAluno($id);
        $turmas = Turma::getAll($aluno[0]['curso']);
        $observacoes = Observacao::getById($id);
        return view('curso.aluno', ['aluno' => $aluno[0], 'turmas' => $turmas, 'observacoes' => $observacoes]);
    }

    public function removerAluno($id, $turma)
    {
        Aluno::removeAluno($id);
        return redirect('curso/turma/' . $turma)->with("success", "Aluno Removido da turma");
    }

    public function salvaObservacao(Request $request){
        Observacao::create($request->all());
        return redirect('curso/aluno/'.$request->aluno_id);
    }
}
