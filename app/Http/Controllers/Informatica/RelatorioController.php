<?php

namespace App\Http\Controllers\Informatica;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Curso;
use App\Models\PresencaAula;
use App\Models\Turma;
use App\Models\Aluno;
use App\Models\ListaEspera;

use App\Exports\RelatorioExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class RelatorioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->curso = new Curso();
        $this->presencaAula = new PresencaAula();
        $this->turma = new Turma();
        $this->aluno = new Aluno();
        $this->listaEspera = new ListaEspera();
    }

    public function index($curso_id)
    {
        $curso = Curso::get($curso_id);
        $turmas = Turma::getAll($curso_id);
        $lista = ListaEspera::countLista($curso_id);
        return view('curso.relatorios', ["curso"=>$curso, "turmas" => $turmas, "lista" => $lista]);
    }

    public function relatorios($curso) 
    {   
        $result = PresencaAula::getAll($curso);
        return $result;
    }

    public function relatorioPresenca(Request $request)
    {   
        return Excel::download(new RelatorioExport($request->turma), 'RelatorioTurma.xlsx');

    }

}
