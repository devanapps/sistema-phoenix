<?php

namespace App\Http\Controllers\informatica;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Turma;
use App\Models\Aluno;
use App\Models\PresencaAula;
use App\Models\Curso;

class TurmaController extends Controller
{
    private $turma;

    public function __construct()
    {
        $this->middleware('auth');
        $this->turma = new Turma();
        $this->aluno = new Aluno();
        $this->aula  = new PresencaAula();
        $this->curso = new Curso();
    }

    public function listaTurmas($curso){
        $turmas = Turma::getAll($curso);
        $curso = Curso::get($curso);
        return view('curso.index', ['turmas' => $turmas,'curso' => $curso]);
    }

    public function turma($id){
        $turma = Aluno::listaTurma($id);
        $cod_turma = Turma::getCodTurma($id);
        return view('curso.turma',['turma' => $turma,'cod_turma' => $cod_turma,'idTurma' => $id]);
    }

    public function novaTurma($cursoId){
        $curso = Curso::get($cursoId);
        
        $aux = Turma::buscarCodigo($cursoId);

        $codigo = substr($aux[0]['cod_turma'],-1);
        $ano = substr($aux[0]['cod_turma'],0,-2);
        $codigo = $codigo + 1;

        $novoCodigo = $ano . "0" . $codigo;

        return view('curso.novaTurma', ['curso' => $curso, 'novoCodigo' => $novoCodigo]);
    }

    public function salvarTurma(Request $request){
        Turma::create($request->all());
        return redirect('curso/turmas/'.$request->curso)->with("success", 'Turma Cadastrada');
    }

    public function aula($cod_turma){
        $alunos = Aluno::listaTurma($cod_turma);
        $turma  = Turma::getTurma($cod_turma);

        $hoje = date("Y-m-d");   
        $auxHoje = date('l');

        $presencas = PresencaAula::getPresencas($hoje,$cod_turma);

        return view('curso.aula',['turma'=>$turma,'alunos'=>$alunos,'data'=>$hoje,'presencas'=>$presencas]);
    }

    public function getAula($data,$cod_turma)
    {
        $presencas = PresencaAula::getPresencas($data,$cod_turma);
        return $presencas;
    }

    public function salvarAula(Request $request){
        // pensar como salvar a aula
        $data = $request->data;
        $turmaId = $request->turmaId;

        foreach ($request->presentes as $p) {
            $params = [
                "data" => $data,
                "turmaId" => $turmaId,
                "matricula" => $p,
                "presente" => 1
            ];
            PresencaAula::create($params);
        }
        foreach ($request->ausentes as $p) {
            $params = [
                "data" => $data,
                "turmaId" => $turmaId,
                "matricula" => $p,
                "presente" => 0
            ];
            PresencaAula::create($params);
        }
        //return redirect('informatica/aula/'.$request->turmaId)->with("success",'Registros Salvos');
    }
} 
