<?php

namespace App\Http\Controllers\Informatica;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Curso;
use App\Models\ListaEspera;
use phpDocumentor\Reflection\Types\This;

class ListaEsperaController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->curso = new Curso();
        $this->listaEspera = new ListaEspera();
    }

    public function index($curso_id) 
    {
        $curso = Curso::get($curso_id);
        $lista = ListaEspera::get($curso_id);
        return view('curso.listaEspera', ["curso" => $curso, "lista"=>$lista]);
    }

    public function listaDeEspera($curso)
    {
        $curso = Curso::get($curso);
        return view('curso.salvarListaEspera', ["curso" => $curso]);
    }

    public function salvar(Request $request)
    {
        $lista = ListaEspera::create($request->all());
        return redirect('curso/listaEspera/'.$request->curso)->with("success", 'Salvo na lista de Espera');
    }
}

?>