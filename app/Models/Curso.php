<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = [
        'id',
        'nome'
    ];

    protected $table = 'curso';

    public static function get($id){
        return static::find($id);
    }

}
?>