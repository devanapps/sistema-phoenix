<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = [
        'id',
        'cod_turma',
        'horario',
        'curso'
    ];

    protected $table = 'turmas';

    public function Aluno(){
        return $this->hasMany(Aluno::class,'turmaId');
    }

    public static function getAll($curso){
        return static::with('Aluno')->where('curso','=',$curso)->get();
    }

    public static function getAlunos($id){
        return static::with('Aluno')->find($id);
    }

    public static function getCodTurma($id){
        return static::select('cod_turma')->where('id','=',$id)->get();
    }

    public static function getTurma($id){
        return static::find($id);
    }

    public static function buscarCodigo($id){
        return static::where('curso','=',$id)->orderBy('curso','desc')->limit(1)->get();
    }

}
