<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = [
        'id',
        'turmaId',
        'matriculaSocio',
    ];

    protected $table = 'alunos';

    // public function TurmaInformatica(){
    //     return $this->belongsTo(TurmaInformatica::class,'turmaId');
    // }
    public function Socio()
    {
        return $this->belongsTo(Socio::class, 'matriculaSocio');
    }

    public static function listaTurma($id)
    {
        return static::selectRaw('alunos.id,alunos.turmaId,status.descricao, socio.matricula,socio.nome,socio.numeroBeneficio,socio.status,(select COUNT(1) as faltas From aula a where a.presente = 0 and a.matricula = socio.matricula and a.turmaId = ' . $id . ') as faltas, (select COUNT(1) as presencas From aula a where a.presente = 1 and a.matricula = socio.matricula and a.turmaId = ' . $id . ') as presencas')
            ->join('socio', 'alunos.matriculaSocio', '=', 'socio.matricula')
            ->join('status', 'socio.status', '=', 'status.codigo')
            ->where('turmaId', '=', $id)->get();
    }

    public static function verificaAluno($matricula)
    {
        $result = static::where('matriculaSocio', '=', $matricula)->get();
        return $result;
    }

    public static function transferir($id, $turma)
    {
        return static::where('id',$id)->update(['turmaId' => $turma]);
    }

    public static function buscaAluno($id)
    {
        return static::selectRaw('alunos.*,status.descricao,socio.matricula,socio.nascimento, 
        socio.nome,socio.cpf,socio.numeroBeneficio,socio.status,turmas.curso,
        (select COUNT(1) as faltas From aula a where a.presente = 0 and a.matricula = socio.matricula and a.turmaId = ' . $id . ') as faltas, 
        (select COUNT(1) as faltas From aula a where a.presente = 1 and a.matricula = socio.matricula and a.turmaId = ' . $id . ') as presenca')
            ->join('socio', 'alunos.matriculaSocio', '=', 'socio.matricula')
            ->join('status', 'socio.status', '=', 'status.codigo')
            ->join('turmas', 'alunos.turmaId', '=', 'turmas.id')
            ->where('alunos.id', '=', $id)->get();
    }

    public static function removeAluno($id)
    {
        return static::where('id', $id)->delete();
    }
}
