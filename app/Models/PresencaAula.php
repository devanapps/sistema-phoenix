<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresencaAula extends Model
{
    protected $fillable = [
        'data',
        'matricula',
        'presente',
        'turmaId'
    ];

    protected $table = 'aula';

    public $timestamps = false;

    public static function getPresencas($data,$id){
        return static::where([['data','=',$data],['turmaId','=',$id],])->get();
    }

    public static function getAll($curso){
        $presenca = static::selectRaw("count(1) as presencas")
        ->join('turmas','turmaId','=','turmas.id')
        ->where([['turmas.curso','=',$curso],['presente','=',1],])
        ->get();

        $faltas = static::selectRaw("count(1) as faltas")
        ->join('turmas','turmaId','=','turmas.id')
        ->where([['turmas.curso','=',$curso],['presente','=',0],])
        ->get();

        $result = array(
            "presencas" => $presenca[0]['presencas'],
            "faltas" => $faltas[0]['faltas']
        );
        
        return $result;
    }

}
