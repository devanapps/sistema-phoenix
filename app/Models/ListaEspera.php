<?php   

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListaEspera extends Model
{
    protected $fillable = [
        'id',
        'matricula',
        'curso',
    ];

    protected $table = 'lista_espera';

    public static function get($curso) {
        return static::selectRaw('lista_espera.*, socio.nome, curso.nome as curso')
            ->join('socio','lista_espera.matricula','=','socio.matricula')
            ->join('curso','lista_espera.curso','=','curso.id')
            ->where('curso', '=', $curso)
            ->orderBy('created_at', 'asc')
            ->get();
    }

    public static function countLista($curso)
    {
        $lista = static::selectRaw("count(1) as lista")
        ->where([['curso','=',$curso]])
        ->get();
        return $lista[0]['lista'];
    }
}
?>