<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    protected $fillable = [
        'id',
        'matricula',
        'nome',
        'cpf',
        'numeroBeneficio',
    ];

    protected $table = 'socio';

    public static function getSocio($matricula){
        return static::selectRaw('socio.*,status.descricao')
        ->join('status','socio.status','=','status.codigo')
        ->where('matricula','=',$matricula)->get();
    }

    public static function get($id){
        return static::selectRaw('socio.*,status.descricao')
        ->join('status','socio.status','=','status.codigo')
        ->where('id','=',$id)->get();
    }

    public static function getMensalidades() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://anapps.herokuapp.com/repasse?numeroBeneficio=168.783.640-7");
        $result = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($result, true);
        return $result;
    }

    public static function getAll(){
        return static::selectRaw('socio.*,status.descricao')
        ->join('status','socio.status','=','status.codigo')
        ->get();
    }
}
