<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Observacao extends Model
{
    protected $fillable = [
        'id',
        'aluno_id',
        'observacao'
    ];

    protected $table = 'observacoes';

    public static function getById($aluno){
        $result = static::where('aluno_id','=',$aluno)->get();
        return $result;
    }
}
