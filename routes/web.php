<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/socio/{id}','SocioController@consultaSocio');
Route::get('/listaSocio','SocioController@listaSocio');
Route::group(["prefix" => "curso"], function () {
    Route::get('/turmas/{id}',"Informatica\TurmaController@listaTurmas");
    Route::get('/novaTurma/{curso}',"Informatica\TurmaController@novaTurma");
    Route::get('/turma/{id}',"Informatica\TurmaController@turma");
    Route::get('/matricularAluno/{id}',"Informatica\AlunoController@matricularAluno");
    Route::get('/aula/{id}',"Informatica\TurmaController@aula");
    Route::post('/buscaAluno',"Informatica\AlunoController@buscarAluno");
    Route::post('/salvarAluno',"Informatica\AlunoController@salvarAluno");
    Route::get('/removerAluno/{id}/{turma}',"Informatica\AlunoController@removerAluno");
    Route::get('/transferirAluno/{id}/{turma}',"Informatica\AlunoController@transferirAluno");
    Route::post("/salvarAula",'Informatica\TurmaController@salvarAula');
    Route::get('/buscaPresenca/{data}/{id}', 'Informatica\TurmaController@getAula');
    Route::get('/aluno/{id}', 'Informatica\AlunoController@detalheAluno');
    Route::post('/salvarTurma','Informatica\TurmaController@salvarTurma');
    Route::post('/salvarObservacao', 'Informatica\AlunoController@salvaObservacao');
    Route::get('/listaEspera/{id}','Informatica\ListaEsperaController@index');
    Route::get('/alunoListaEspera/{id}','Informatica\ListaEsperaController@listaDeEspera');
    Route::post('/salvarListaEspera','Informatica\ListaEsperaController@salvar');
    Route::get('/relatorios/{curso}','Informatica\RelatorioController@index');
    Route::get('/buscaRelatorio/{curso}','Informatica\RelatorioController@relatorios');
    Route::post('/gerarRelatorio','Informatica\RelatorioController@relatorioPresenca');
});

