@extends('adminlte::page')

@section('title', 'Sócio')

@section('content_header')
<h1>Sócios</h1>
@stop

@section('content')
    
    <div class="box box-success">
        <div class="container-fluid">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Matricula</th>
                                <th>Nome</th>
                                <th>Cpf</th>
                                <th>Benefício</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($socios as $s)
                        <tr class='clickable-row' data-href="{{url('socio/' . $s->id)}}">
                                <td>{{$s->matricula}}</td>
                                <td>{{$s->nome}}</td>
                                <td>{{$s->cpf}}</td>
                                <td>{{$s->numeroBeneficio}}</td>
                                <td>{{$s->status}} - {{$s->descricao}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
        </div>
    </div>
    <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    "language": {
                        "lengthMenu": "Exibindo  por página _MENU_",
                        "zeroRecords": "Resultado não encontrado",
                        "info": "Exibindo página _PAGE_ de _PAGES_",
                        "infoEmpty": "Sem Resultados encontrados",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "search": "Pesquisar",
                        "paginate": {
                            "first":      "Primeira",
                            "last":       "Última",
                            "next":       "Próximo",
                            "previous":   "Anterior"
                        },
                    }
                });
                $(".clickable-row").click(function() {
                    window.location = $(this).data("href");
                });
            } );
    
           
        </script>
    <style>
        .container-fluid{
            margin-top: 2%;
        }
        .clickable-row{
            cursor: pointer;
        }
    </style>     
@stop

