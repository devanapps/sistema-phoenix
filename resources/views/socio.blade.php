@extends('adminlte::page')

@section('title', 'Sócio')

@section('content_header')
<h1>Sócio</h1>
@stop

@section('content')
    
    <div class="box box-success">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-2">
                    <h4>Matrícula</h4>
                    <p>{{$socio->matricula}}</p>
                </div>
                <div class="col-lg-3">
                    <h4>Nome</h4>
                    <p>{{$socio->nome}}</p>
                </div>
                <div class="col-lg-3">
                    <h4>CPF</h4>
                    <p>{{$socio->cpf}}</p>
                </div>
                <div class="col-lg-3">
                    <h4>Número do Benefício</h4>
                    <p>{{$socio->numeroBeneficio}}</p>
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-2">
                    <h4>Nascimento</h4>
                    <p>{{ date('d/m/Y', strtotime($socio->nascimento)) }}</p>
                </div>
                <div class="col-lg-3">
                    <h4>Status</h4>
                    <p>{{$socio->status}} - {{$socio->descricao}}</p>
                </div>
                <div class="col-lg-3">
                    <h4>Valor Benefício</h4>
                    <p>{{$socio->valorBeneficio}}</p>
                </div>
                <div class="col-lg-3">
                    <h4>Percentual Desconto</h4>
                    <p>{{$socio->percentual}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-success ">
        <div class="container-fluid">
            <h4>Mensalidades</h4>
            <div class="list-group col-lg-6 obs">
                @foreach ($mensalidades as $m)
                    <a class="list-group-item ">
                        @php
                            $aux = array("T", "Z");
                            $data = str_replace($aux," ",$m['competencia']);
                        @endphp
                        <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                        <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                    </a>
                    <a class="list-group-item ">
                            
                            <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                            <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                        </a>
                        <a class="list-group-item ">
                                
                                <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                            </a>
                            <a class="list-group-item ">
                                
                                    <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                    <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                </a>
                                <a class="list-group-item ">
                                
                                        <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                        <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                    </a>
                                    <a class="list-group-item ">
                                
                                            <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                            <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                        </a>
                                        <a class="list-group-item ">
                                
                                                <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                                <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                            </a>
                                            <a class="list-group-item ">
                                
                                                    <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                                    <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                                    
                                                </a>
                                                <a class="list-group-item ">
                                
                                                        <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                                        <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                                    </a>
                                                    <a class="list-group-item ">
                                
                                                            <h4 class="list-group-item-heading">Data: {{ date('d/m/Y', strtotime($data))}}</h4>
                                                            <p class="list-group-item-text">Valor: {{$m['valor']}}</p>
                                                        </a>
                @endforeach
            </div>
            <div class="col-lg-5 float-left">
                <h4>Valor total pago:</h4>
                <p>R$ 598,00</p>

                <h4>Quantidade de mensalidades:</h4>
                <p>6</p>
            </div>
        </div>
    </div>
@stop
<style>
        .list-group-item {
            border: 0;
        }
    
        .obs {
            overflow-y: scroll;
            overflow-x: hidden;
            max-height: 200px;
        }
    
        
    </style>