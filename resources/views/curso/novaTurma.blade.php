@extends('adminlte::page')

@section('title', 'Nova Turma')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
        <h3 class="box-title">Nova Turma - {{$curso->nome}}</h3>
        </div>
        <div class="box-body">
        <form class="col-lg-10" method="POST" action="{{url('curso/salvarTurma')}}">
            {{ csrf_field() }}
        <input type="hidden" name="curso" value="{{$curso->id}}">
            <div class="form-group">
              <label for="exampleFormControlInput1">Código Turma</label>
            <input type="text" name="cod_turma" class="form-control" id="" value="{{$novoCodigo}}" readonly>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Horário</label>
              <input type="time" name="horario" id="" class="form-control">
            </div>
            <input type="submit" class="btn btn-success" value="Salvar">
          </form>
          
        </div>
    </div>
@stop