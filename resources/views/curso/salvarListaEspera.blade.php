@extends('adminlte::page')

@section('title', 'Matricular Aluno')

@section('content_header')
<h1>Adicionar na lista de espera {{$curso->nome}}</h1>
@stop

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="box box-success">
    <div class="box-body">
        <form class="col-lg-12" method="POST" action="{{url('curso/salvarListaEspera')}}">
            {{ csrf_field() }}
        <input type="hidden" name="curso" value="{{$curso->id}}">
            <div class="form-group col-lg-2">
                <label for="">Matrícula Sócio</label>
                <input type="text" name="matricula" class="form-control" name="matricula" id="matricula"
                    placeholder="000.000">
            </div>
            <div class="lds-dual-ring col-lg-1"></div>

            <div class="form-group col-lg-9">
                <label for="">Nome</label>
                <input type="text" class="form-control" id="nome" placeholder="Nome do sócio" readonly>
            </div>
            <div class="form-group col-lg-3">
                <label for="">Número do Benefício</label>
                <input type="text" class="form-control" id="nb" readonly>
            </div>
            <div class="form-group col-lg-3">
                <label for="">CPF</label>
                <input type="text" class="form-control" id="cpf" readonly>
            </div>
            <div class="form-group col-lg-3">
                <label for="">Status</label>
                <input type="text" class="form-control" id="status" readonly>
            </div>
            
            <div class="form-group col-lg-6">
                <input type="submit" id="btn-envia" class="btn btn-success" value="Salvar">
            </div>
        </form>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#matricula").focusout(function (e) {
        e.preventDefault();
        $('.lds-dual-ring').css('visibility', 'visible');
        var matricula = $('#matricula').val();

        $.ajax({
            type: 'POST',
            url: '/curso/buscaAluno',
            data: { matricula: matricula },
            success: function (data) {
                $('.lds-dual-ring').css('visibility', 'hidden');
                var result = jQuery.parseJSON(data);
                $('#nome').val(result[0]['nome']);
                $('#nb').val(result[0]['numeroBeneficio']);
                $('#cpf').val(result[0]['cpf']);
                $('#status').val(result[0]['descricao']);
                var status = result[0]['status'];
                if (status != '00' && status != '09' && status != '13' && status != '28') {
                    $('#btn-envia').attr('disabled', 'disabled');
                    alert("Sócio possui pendênia!");
                }
            }
        });
    });
</script>
<style>
    .lds-dual-ring {
        visibility: hidden;
        width: 64px;
        height: 64px;
    }

    .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 46px;
        height: 46px;
        margin: 17px -5px 1px;
        border-radius: 50%;
        border: 5px solid #000;
        border-color: #000 transparent #000 transparent;
        animation: lds-dual-ring 1.2s linear infinite;
    }

    @keyframes lds-dual-ring {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>
@stop