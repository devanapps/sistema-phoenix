@extends('adminlte::page')

@section('title', 'Matricular Aluno')

@section('content_header')
<h1>Lista de Espera {{$curso->nome}}</h1>
<a class="btn btn-success pull-right" href="{{url("curso/alunoListaEspera/" . $curso->id)}}">Adicionar</a>

@stop

@section('content')
<div class="box box-success">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">Matricula</th>
            <th scope="col">Nome</th>
            <th scope="col">Curso</th>
            <th scope="col">Data</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($lista as $l)
            <tr class='clickable-row' data-href="">
             <td>{{$l->matricula}}</td>
             <td>{{$l->nome}}</td>
             <td>{{$l->curso}}</td>
             <td>{{date('d/m/Y', strtotime($l->created_at))}}</td>
            </tr>
        @endforeach
    </tbody>
       
</div>
@if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
        
    </div>
@endif
<style>
.clickable-row{
cursor: pointer;
}
</style>
<script>
jQuery(document).ready(function($) {
$(".clickable-row").click(function() {
    window.location = $(this).data("href");
});
});
</script>
@stop