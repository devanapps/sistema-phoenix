@extends('adminlte::page')

@section('title', '')

@section('content_header')
    <h1>Turmas {{$curso->nome}}</h1>
    <a class="btn btn-success pull-right" href="{{url("curso/novaTurma/" . $curso->id)}}">Nova turma</a>
@stop

@section('content')
    <div class="box box-success">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Codigo da turma</th>
                <th scope="col">Horário</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($turmas as $t)
                <tr class='clickable-row' data-href="{{ url('curso/turma/' . $t->id) }}">
                 <td>{{$t->cod_turma}}</td>
                 <td>{{$t->horario}}</td>
                </tr>
            @endforeach
        </tbody>
           
    </div>
<style>
.clickable-row{
    cursor: pointer;
}
</style>
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>
    @stop