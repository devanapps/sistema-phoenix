@extends('adminlte::page')

@section('title', 'Visualizar turma')

@section('content_header')
    <h1>Turma @foreach ($cod_turma as $c)
        {{$c->cod_turma}}
    @endforeach</h1>
    <a class="btn btn-success pull-right" href="{{url("curso/matricularAluno/".$idTurma)}}">Matricular Aluno</a>
    <a class="btn btn-info pull-right" href="{{url("curso/aula/".$idTurma)}}">Diário de Aula</a>
@stop

@section('content')
    <div class="box box-success">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Matricula Socio</th>
                <th scope="col">Nome</th>
                <th scope="col">Numero do Beneficio</th>
                <th scope="col">Status</th>
                <th scope="col">Faltas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($turma as $t)
                @php
                $color = "";
                   if ($t->status != '00' && $t->status != '09' && $t->status != '13' && $t->status != '28'){
                    $color = "red";
                   } 
                @endphp
               
                <tr class='clickable-row' style="color:{{$color}}"  data-href="{{ url('curso/aluno/' . $t->id) }}">
                 <td>{{$t->matricula}}</td>
                 <td>{{strtoupper($t->nome)}}</td>
                 <td>{{$t->numeroBeneficio}}</td>
                 <td>
                     {{$t->status .' - '.$t->descricao}}
                 </td>
                 <td>{{$t->faltas}}</td>
                </tr>
            @endforeach
        </tbody>
        </table>
    </div>

    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
@endif
<style>
.clickable-row{
    cursor: pointer;
}
.btn-info{
    margin-right: 2%;
}
</style>
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>
@stop