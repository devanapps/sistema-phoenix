@extends('adminlte::page')

@section('title', 'Relatórios')

@section('content_header')
<h1>Relatórios - {{$curso->nome}}</h1>
<meta name="csrf-token" content="{{ csrf_token() }}" />

@stop

@section('content')

<div class="container-fluid">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            {{-- <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li> --}}
                        </ul>
                    </div>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                            class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <p class="text-center">
                            <strong>Presenças: 1 Jan, 2019 - 30 Jun, 2019</strong>
                        </p>

                        <!-- Sales Chart Canvas -->
                        <canvas id="presencasChart" style="height: 300px; width: 704px;" width="704"
                            height="300px"></canvas>
                        <!-- /.chart-responsive -->
                    </div>

                    <!-- /.col -->
                    <div class="col-md-4">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="glyphicon glyphicon-ok"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Alunos Matriculados</span>
                                <span class="info-box-number">55</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </div>

                            <!-- /.info-box-content -->
                        </div>
                        <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class=" glyphicon glyphicon-list-alt"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Em Lista de Espera</span>
                                <span class="info-box-number">{{$lista}}</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </div>

                        </div>

                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="glyphicon glyphicon-remove"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Desistências</span>
                                <span class="info-box-number">10</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="col-lg-5">
                        <h4>Relatório por turmas</h4>
                        <form action="{{url('curso/gerarRelatorio')}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$curso->id}}" name="curso">
                            <select class="form-control turma" name="turma" id="">
                                <option value="">Turma</option>
                                @foreach ($turmas as $t)
                                    <option value="{{$t->id}}">{{$t->cod_turma}}</option>
                                @endforeach
                            </select>
                            <input class="btn btn-success envia float-right" type="submit" value="Gerar">
                        </form>
                    </div>
                </div>

            </div>

            <!-- /.box -->
        </div>
    </div>
</div>
<style>
    .turma {
        width: 50%;
        display: inline-flex;
        float: left;
    }
</style>
<!-- ChartJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<script>
    var presencas = 0;
    var faltas = 0;
    $.ajax({
        type: 'GET',
        url: '/curso/buscaRelatorio/1',
        success: function (data) {
            presencas = data['presencas'];
            faltas = data['faltas'];
            console.log(faltas);
            renderChart();
        }
    });
    function renderChart() {
        var ctx = document.getElementById('presencasChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Faltas', 'Presenças'],
                datasets: [{
                    label: '# presenças',
                    data: [faltas, presencas],
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(54, 162, 235)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1
                }]
            },

        });
    }
</script>
@stop