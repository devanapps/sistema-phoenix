@extends('adminlte::page')

@section('title', 'Aluno')

@section('content_header')
<h1>Aluno</h1>

<a class="btn btn-danger pull-right"
    href="{{ url('curso/removerAluno/' . $aluno->id . '/'.$aluno->turmaId) }}">Remover</a>
<a class="btn btn-info pull-right" href="" data-toggle="modal" data-target="#myModal"> Transferir</a>
@stop

@section('content')
<div class="box box-success">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-2">
                <h4>Matrícula</h4>
                <p>{{$aluno->matriculaSocio}}</p>
            </div>
            <div class="col-lg-3">
                <h4>Nome</h4>
                <p>{{$aluno->nome}}</p>
            </div>
            <div class="col-lg-3">
                <h4>CPF</h4>
                <p>{{$aluno->cpf}}</p>
            </div>
            <div class="col-lg-3">
                <h4>Número do Benefício</h4>
                <p>{{$aluno->numeroBeneficio}}</p>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-2">
                <h4>Nascimento</h4>
                <p>{{ date('d/m/Y', strtotime($aluno->nascimento)) }}</p>
            </div>
            <div class="col-lg-3">
                <h4>Presenças</h4>
                <p>{{$aluno->presenca}}</p>
            </div>
            <div class="col-lg-3">
                <h4>Faltas</h4>
                <p>{{$aluno->faltas}}</p>
            </div>
            <div class="col-lg-3">
                <h4>Status</h4>
                <p>{{$aluno->status}} - {{$aluno->descricao}}</p>
            </div>
        </div>
    </div>
</div>
<div class="box box-success obs">
    <div class="container-fluid">
        <h4>Observações</h4>
        <div class="list-group col-lg-11">
            @foreach ($observacoes as $o)
                <a class="list-group-item ">
                    <h4 class="list-group-item-heading">{{ date('d/m/Y', strtotime($o->created_at))}}</h4>
                    <p class="list-group-item-text">{{$o->observacao}}</p>
                </a>
            @endforeach
        </div>
    </div>
</div>
<a class="btn btn-info pull-right" href="" data-toggle="modal" data-target="#myModal2">Nova</a>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transferir aluno</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Codigo da turma</th>
                            <th scope="col">Horário</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($turmas as $t)
                        @if ($aluno->turmaId != $t->id)

                        <tr class='clickable-row' data-href="{{ url('curso/turma/' . $t->id) }}">
                            <td>{{$t->cod_turma}}</td>
                            <td>{{$t->horario}}</td>
                            <td>
                                <a href="{{ url('/curso/transferirAluno/'. $aluno->id . '/' . $t->id) }}"
                                    class="btn btn-success">Transferir</a>
                            </td>
                        </tr>
                        @endif

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nova Observação</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('curso/salvarObservacao')}}" method="POST" class="">
                        {{ csrf_field() }}
                    <input type="hidden" name="aluno_id" value="{{$aluno->id}}">
                    <div class="form-group">
                        <label for="">Observação</label>
                        <textarea name="observacao" class="form-control" cols="30" rows="10"></textarea>
                    </div> 
                    <input type="submit" value="Salvar" class="btn btn-success">
                </form>
            </div>
        </div>

    </div>
</div>

<style>
    .list-group-item {
        border: 0;
    }

    .obs {
        overflow-y: scroll;
        overflow-x: hidden;
        max-height: 400px;
    }

    .btn-info {
        margin-right: 2%;
    }
</style>
@stop