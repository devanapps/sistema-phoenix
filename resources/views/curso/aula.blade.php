@extends('adminlte::page')

@section('title', 'Diário de Aula')

@section('content_header')
<h1>Aula Turma {{$turma->cod_turma}}</h1>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content')
<div class="box box-success">
<form action="{{url('curso/salvarAula')}}" method="POST">
            {{ csrf_field() }}
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    <div class="box-header with-border">
    Data Aula: <input type="date" class="form-control" name="dataAula" id="dataAula" value="{{$data}}">
    <input type="button" class="btn btn-success pull-right" value="Salvar"> 
    <input type="hidden" name="turmaId" id="turmaId" value="{{$turma->id}}">  
    </div>
    <div class="box-body">
            <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Matricula Socio</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Presente</th>
                        <th scope="col">Ausente</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($alunos as $t)
                        @php
                        $color = "";
                           if ($t->status != '00' && $t->status != '09' && $t->status != '13' && $t->status != '28'){
                            $color = "red";
                           } 
                        @endphp

                        <tr class='clickable-row' style="color:{{$color}}"  data-href="">
                         <td>{{$t->id}}</td>
                         <td>{{$t->matricula}}</td>
                         <td>{{strtoupper($t->nome)}}</td>
                         <td class="presenca"> <input type="radio" name="{{$t->id}}" class="presente" value="{{$t->matricula}}" id=""> </td>
                         <td class="presenca"> <input type="radio" name="{{$t->id}}" class="ausente" value="{{$t->matricula}}" id=""> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</form>
</div>
@if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
@endif
<style>
#dataAula{
    width: 15%;
    display: inline-flex;
    margin-left: 1%;
}
.presenca{
    width:6%;
    text-align: center;
}
.lds-ring {
    display: inline-block;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 999999;
    background-color: #00000036;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 51px;
  height: 51px;
  margin: 6px;
  border: 6px solid #00a65a;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #00a65a transparent transparent transparent;
  left: calc(50% - 51px);
  top: calc(50% - 51px);
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

</style>
<script>
$(document).ready(function(){
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('.btn-success').click(function(){
        var turmaId = $('#turmaId').val();
        var data = $('#dataAula').val();
        var presentes = new Array();
        var ausentes = new Array();
        $("input[class='presente']:checked").each(function ()
        {
            // valores inteiros usa-se parseInt
            //checkeds.push(parseInt($(this).val()));
            // string

            presentes.push( $(this).val());
        });
        $("input[class='ausente']:checked").each(function ()
        {
            // valores inteiros usa-se parseInt
            //checkeds.push(parseInt($(this).val()));
            // string

            ausentes.push( $(this).val());
        });
        $('.lds-ring').show();
        $.ajax({
            type:'POST',
            url:'/curso/salvarAula',
            data:{
                turmaId:turmaId,
                data:data,
                presentes:presentes,
                ausentes:ausentes
            },
            success:function(data){
                alert("Registros Salvos");
                $('.lds-ring').hide();
            }
        });
    });

    function buscaAula(){
        $('.lds-ring').show();
        $("input[type='radio']").removeAttr("checked");
        $(".presente").removeAttr("checked");
        $(".ausente").removeAttr("checked");
        $.ajax({
            type:'GET',
            url:'/curso/buscaPresenca/' + $('#dataAula').val() + '/' + $('#turmaId').val(),
            success:function(data){
                if(data['length'] != 0){
                    data.forEach(d => {
                        if (d['presente'] == 1){
                            $(".presente[value*='"+d['matricula']+"']").attr("checked","checked");
                        } else {
                            $(".ausente[value*='"+d['matricula']+"']").attr("checked","checked");
                        }
                    });
                } else {
                    $(".presente").removeAttr("checked");
                    $(".ausente").removeAttr("checked");
                }
                $('.lds-ring').hide();
            }
        });
    }
    buscaAula();

    $('#dataAula').change(function(){
        buscaAula();
    })

});
</script>
@stop
